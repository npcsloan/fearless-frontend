function createCard(name, description, pictureUrl, dates, subtitle) {
    return `
    <div class="card mb-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
      <span>${dates}</span>
      </div>
    </div>
  `;
}

function formatDate(originalDate) {
    const date = new Date(originalDate);
    const formattedDate = date.toLocaleDateString('en-US', {
        month: 'numeric',
        day: 'numeric',
        year: 'numeric',
    });
    return formattedDate;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const container = document.querySelector(`#conference-grid-container`);

    try {
        const response = await fetch(url);

        if (!response.ok) {
            container.innerHTML += `<div class="alert alert-danger" role="alert">
                Unable to retreive conferences data from ${url}
            </div>`;

        } else {
            const data = await response.json();

            let columnNumber = 1

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = formatDate(details.conference.starts)
                    const endDate = formatDate(details.conference.ends)
                    const dates = `${startDate} - ${endDate}`
                    const subtitle = details.conference.location.name

                    const html = createCard(name, description, pictureUrl, dates, subtitle);
                    const column = document.querySelector(`#col${columnNumber}`);
                    column.innerHTML += html;
                    if (columnNumber === 3) {
                        columnNumber = 1
                    } else {
                        columnNumber += 1
                    }
                }
            }

        }
    } catch (e) {
        container.innerHTML += `<div class="alert alert-danger" role="alert">
        An error occurred: ${e.message}
    </div>`;
    }

});
